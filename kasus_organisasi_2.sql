//find CEO
SELECT nama FROM employee WHERE atasan_id IS NULL

//find Staff
SELECT nama FROM employee WHERE atasan_id = 5

//find Director
SELECT nama FROM employee WHERE atasan_id = 2

//find Manager
SELECT nama FROM employee WHERE atasan_id = 4

//mencari bawahan pakbudi
SELECT nama FROM employee WHERE atasan_id IS NOT NULL

//mencari bawahan bu sinta
SELECT nama FROM employee WHERE atasan_id = 4
