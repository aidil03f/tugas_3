INSERT INTO `karyawan` (`id`, `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) 
	VALUES (NULL, 'Rizki Saputra', 'L', 'Menikah', '1980-11-10', '2011-01-01', '1'), 
	       (NULL, 'Farhan Reza', 'L', 'Belum', '1989-01-11', '2011-01-01', '1'),
               (NULL, 'Riyando Adi', 'L', 'Menikah', '1977-01-25', '2011-01-01', '1'),
               (NULL, 'Diego Manuel', 'L', 'Menikah', '1983-02-22', '2012-04-09', '2'),
               (NULL, 'Satya Laksana', 'L', 'Menikah', '1981-12-01', '2011-03-19', '2'),
               (NULL, 'Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-06-15', '2'),
               (NULL, 'Putri Persada', 'P', 'Menikah', '1988-01-30', '2013-04-14', '2'),
               (NULL, 'Alma Safira', 'P', 'Menikah', '1991-05-18', '2013-09-28', '3'),
               (NULL, 'Haqi Hafiz', 'L', 'Belum', '1995-09-19', '2015-09-03', '3'),
               (NULL, 'Abi Isyawara', 'L', 'Belum', '1991-03-06', '2012-01-22', '3'),
               (NULL, 'Maman Kresna', 'L', 'Belum', '1993-08-21', '2012-09-15', '3'),
               (NULL, 'Nadia Aulia', 'P', 'Belum', '1989-07-10', '2012-07-05', '4'),
               (NULL, 'Mutiara Rezki', 'P', 'Menikah', '1988-03-23', '2013-05-21', '4'),
               (NULL, 'Dani Setiawan', 'L', 'Belum', '1986-11-02', '2014-11-30', '4'),
               (NULL, 'Budi Putra', 'L', 'Belum', '1995-10-23', '2015-03-12', '4')

          
INSERT INTO `departemen` (`id`,`nama`)
VALUES
    (NULL, 'Manajemen'),
    (NULL, 'Pengambangan Bisnis'),
    (NULL, 'Teknisi'),
    (NULL, 'Analis')
